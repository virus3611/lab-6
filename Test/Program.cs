﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Test
{
    public class Student
        {
            string[] allText;

            public double AverageMath { get; private set; }

            public double AveragePhisics { get; private set; }

            public double AverageInformatics { get; private set; }

            string[] lastName;
            string[] name;
            int[] math;
            int[] phisics;
            int[] informatics;

            double[] average;

            public Student(string path)
            {
                allText = File.ReadAllLines(path).Skip(1).ToArray();

                lastName = new string[allText.Length];
                name = new string[allText.Length];

                math = new int[allText.Length];
                phisics = new int[allText.Length];
                informatics = new int[allText.Length];

                average = new double[allText.Length];

                for (int i = 0; i < allText.Length; i++)
                {
                    string[] item = allText[i].Split(' ');

                    lastName[i] = item[0];
                    name[i] = item[1];

                    math[i] = int.Parse(item[2]);
                    phisics[i] = int.Parse(item[3]);
                    informatics[i] = int.Parse(item[4]);

                    average[i] = AverageItem(math[i], phisics[i], informatics[i]);
                }

                AverageMath = math.Average();
                AveragePhisics = phisics.Average();
                AverageInformatics = informatics.Average();
            }

            public double AverageItem(double math, double phisics, double informatics)
            {
                return (math + phisics + informatics) / 3.0;
            }

            public string[] Max()
            {
                var list = new List<string>();

                for (int i = 0; i < allText.Length; i++)
                {
                    if (average[i] == average.Max())
                        list.Add(lastName[i] + " " + name[i] + " ср. балл: " + average[i]);
                }

                return list.ToArray();
            }
        }
    internal class Program
    {
        public static void Main(string[] args)
        {
            Student info = new Student("../../myText.txt");
            Console.WriteLine("Средний балл: ");
            Console.WriteLine("Математика: {0}", info.AverageMath);
            Console.WriteLine("Физика: {0}", info.AveragePhisics);
            Console.WriteLine("Информатика: {0}", info.AverageInformatics);

            Console.WriteLine("Лучшая успеваимость:");
            foreach (var item in info.Max())
            {
                Console.WriteLine(item);
            }

            File.WriteAllLines("../../myText2.txt", info.Max());
        }
    }
}
