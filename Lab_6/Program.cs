﻿using System;

namespace Lab_6
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Time time = new Time();
            Time timeTwo = new Time(12,45,16);
            Console.Write("Хотите ввести дату?(1/0): ");
            int buttonPower = int.Parse(Console.ReadLine());
            if (buttonPower == 1)
            {
                Console.WriteLine("Секунды: ");
                time.setSeconds(int.Parse(Console.ReadLine()));
                Console.WriteLine("Минуты: ");
                time.setMinutes(int.Parse(Console.ReadLine()));
                Console.WriteLine("Часы: ");
                time.setHours(int.Parse(Console.ReadLine()));
                time.OutputTimeFormat();
                Console.Write("Хотите изменить дату?(1/0): ");
                buttonPower = int.Parse(Console.ReadLine());
                if (buttonPower == 1)
                {
                    Console.WriteLine("1: секунды\n2: минуты\n3: часы");
                    int button = int.Parse(Console.ReadLine());
                    switch (button)
                    {
                        case 1:
                            time.ChangeSeconds(int.Parse(Console.ReadLine()));
                            break;
                        case 2:
                            time.ChangeMinutes(int.Parse(Console.ReadLine()));
                            break;
                        case 3:
                            time.ChangeHours(int.Parse(Console.ReadLine()));
                            break;
                    }
                }
                time.OutputTimeFormat();
                timeTwo.OutputTimeFormat();
            }
        }
    }

    public class Time
    {
        private int hours;
        private int minutes;
        private int seconds;

        public Time()
        {
            
        }
        public Time(int hours, int minutes, int seconds)
        {
            setHours(seconds);
            setMinutes(minutes);
            setHours(hours);
        }

        public void setSeconds(int seconds)    //Сеттеры
        {
            if (seconds > 60)
            {
                throw new Exception("Неверный формат!");
            }
                this.seconds = seconds;
        }
        public void setMinutes(int minutes)
        {
            if (minutes > 60)
            {
                throw new Exception("Неверный формат!");
            }
            this.minutes = minutes;
        }
        public void setHours(int hours)
        {
            if (hours > 23)
            {
                throw new Exception("Неверный формат!");
            }
            this.hours = hours;
        }

        public int getSeconds()    //Геттеры
        {
            return seconds;
        }
        public int getMinutes()
        {
            return minutes;
        }
        public int getHours()
        {
            return hours;
        }

        public void OutputTimeFormat()
        {
            Console.WriteLine("Введенное время: {0}:{1}:{2}", getHours(), getMinutes(), getSeconds());
        }

        public void ChangeSeconds(int changeSecond)
        {
            int sec = changeSecond + this.seconds; 
                    int min = sec / 60;
                    ChangeMinutes(min);
                    sec %= 60;
                setSeconds(sec);
        }

        public void ChangeMinutes(int changeMinute)
        {
            int min = changeMinute + this.minutes;
                int hour = (min / 60) + this.hours;
                ChangeHours(hour);
                min %= 60;
            setMinutes(min);
        }

        public void ChangeHours(int changeHour)
        {
            int hour = changeHour + this.hours;
            int min = hour % 24;
            setHours(min);
        }
    }
}