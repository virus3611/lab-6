﻿using System;

namespace ConsoleApplication1
{
    public class Person
    {
        private string name;
        private int age;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get => age;
            set => age = value;
        }
    }

    internal class Program
    {
        public static void Main(string[] args)
        {
            Person personOne = new Person();
            Console.WriteLine(personOne.Age);
            personOne.Age = 20;
            Console.WriteLine(personOne.Age);
            personOne.Name = "Василина";
            Console.WriteLine(personOne.Name);
        }
    }
}