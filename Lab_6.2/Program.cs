﻿using System;

namespace Lab_6._2
{
    internal class Program
    { 
        public static void Main(string[] args)
        {
            Students[] students = new Students[6];
 
            students[0] = new Students("Петров");
            students[0].specialty = Students.Spec.PO;
 
            students[1] = new Students("Иванов");
            students[1].specialty = Students.Spec.RPIS;
 
            students[2] = new Students("Сидоров");
            students[2].specialty = Students.Spec.YITS;
 
            students[3] = new Students("Фролов");
            students[3].specialty = Students.Spec.PO;
 
            students[4] = new Students("Климов");
            students[4].specialty = Students.Spec.RPIS;
 
            students[5] = new Students("Смирнов");
            students[5].specialty = Students.Spec.YITS;
 
// students[6] = new Students("Кузнецов");
// students[6].specialty = Students.Spec.PO;

            Console.Write("Введите кол-во групп: ");
            int groups = int.Parse(Console.ReadLine());
 
            int max = students.Length / groups,
                cursor = 0;
            for (int i = 0; i < groups; i++)
            {
                Console.WriteLine($"======Группа №{i + 1}=======");
// foreach (Students s in students)
                for (int j = 0; j < max; j++, cursor++)
                {
                    students[cursor].Analysis();
                }
 
            }
        } 
    }

    public class Students
    {
        private string name;
        public enum Spec {PO, RPIS, YITS}

        public Spec specialty;

        public Students(string s)
        {
            name = s;
        }

        public void Analysis()
        {
            switch (specialty)
            {
                case Spec.PO:
                    Console.WriteLine( name + ".Группа студента: ПО");
                    break;
                case Spec.RPIS:
                    Console.WriteLine( name + ".Группа студента: РПИС");
                    break;
                case Spec.YITS:
                    Console.WriteLine( name + ".Группа студента: УИТС");
                    break;
            }
        }
    }
}