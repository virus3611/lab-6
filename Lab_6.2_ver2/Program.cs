﻿using System;

namespace Lab_6._2_ver2
{
    class Student
    {
        public enum Profession
        {
            ПО,
            РПИС,
            УИТС
        };
 
        public string surname;

        public Profession prof;

        public Student(string surname, Profession prof)
        {
            this.surname = surname;
            this.prof = prof;
        }
    }

    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введите кол-во учеников: ");
            int countStudents = int.Parse(Console.ReadLine());
 
            Console.Write("Введите кол-во групп: ");
            int countGroups = int.Parse(Console.ReadLine());
 
            Student[] persons = new Student[countStudents];
            Random random = new Random();
 
            // Создаем студентов
            for (int i = 0; i < persons.Length; i++)
            {
                persons[i] = new Student(
                    "Студент №" + (i + 1),
                    (Student.Profession) Enum.Parse(typeof(Student.Profession), Convert.ToString(random.Next(0, 3)))
                );
            }
 
            // Вывод списка студентов
            Console.WriteLine("Список студентов: ");
            for (int i = 0; i < persons.Length; i++)
                EchoStudents(persons[i]);
 
            // Сортировка по группам
            Array.Sort(persons, (student1, student2) => student1.prof.CompareTo(student2.prof));
 
            // Распределение студентов по группам
            Student[][] personsInGroups = new Student[countGroups][];
            double maxPersonsInGroups = Math.Ceiling((double)persons.Length / countGroups);
 
            for (int j = 0; j < countGroups; j++)
            {
                personsInGroups[j] = new Student[(int)maxPersonsInGroups];
            }

            int cursor = 0;
            for (int i = 0; i < maxPersonsInGroups; i++)
            {
                for (int j = 0; (j < countGroups) & (cursor < persons.Length); j++, cursor++)
                {
                    personsInGroups[j][i] = persons[cursor];
                }
            }
 
            // Вывод студентов
            for (int i = 0; i < countGroups; i++)
            {
                Console.WriteLine($"======Группа №{i + 1}=======");
                for (int j = 0; j < personsInGroups[i].Length; j++)
                    EchoStudents(personsInGroups[i][j]);
            }
        }

        private static void EchoStudents(Student person)
        {
            if (person != null)
                Console.WriteLine($"{person.surname} - {person.prof}");
        }
    }
}