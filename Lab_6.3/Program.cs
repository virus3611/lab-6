﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab_6._3
{
    public class Student
    {
        public string lastName;
        public string name;
        public int math;
        public int phisics;
        public int informatics;



        public Student(string line)
        {
//            Console.Write(line);
            string[] item = line.Split(' ');
            lastName = item[0];
            name = item[1];
            math = int.Parse(item[2]);
            phisics = int.Parse(item[3]);
            informatics = int.Parse(item[4]);
        }

        public double AverageItem()
        {
            return (math + phisics + informatics) / 3.0;
        }

       
    }

    internal class Program
        {
            public static void Main(string[] args)
            {
                double informatic = 0, physics = 0, mathematics = 0;
                string max = null;
                string[] allText = File.ReadAllLines("../../myText.txt").Skip(1).ToArray();
                Student[] students = new Student[allText.Length];
                for (int i = 0; i < students.Length; i++)
                {
                    students[i] = new Student(allText[i]);
                    mathematics += students[i].math;
                    physics += students[i].phisics;
                    informatic += students[i].informatics;
                }
                mathematics /= students.Length;
                physics /= students.Length;
                informatic /= students.Length;
                Console.WriteLine("Средний балл: ");
                Console.WriteLine("Математика: {0}", mathematics);
                Console.WriteLine("Физика: {0}", physics);
                Console.WriteLine("Информатика: {0}", informatic);

                Console.WriteLine("Лучшая успеваимость:");
                Array.Sort<Student>(students, (s1, s2) => -s1.AverageItem().CompareTo(s2.AverageItem()));
                Console.WriteLine(students[0].AverageItem());
                max = students[0].AverageItem().ToString();
                File.WriteAllText("../../myText2.txt",students[0].name+  " " + students[0].lastName + " " +  max);
            }
        }
    }