﻿using System;
using System.Xml;

namespace HelpVasilina
{
    /// <summary>
    /// Класс перечислений
    /// Создается отедльно ото всех классов
    /// </summary>
    public enum MarksEnum 
    {
        NotPassed = 0,
        Passed = 1,
        Bad = 2,
        Statisfactory = 3,
        Good = 4,
        Exellent = 5
    }

    public class Subject : System.Attribute
    {
        /// <summary>
        /// Поле атрибутов
        /// </summary>
        private string name;
        private int mark;

        /// <summary>
        /// Конструкторы
        /// </summary>
        /// <param name="name">название предмета</param>
        /// <param name="mark">зачет или экзамен</param>
        public Subject(string name, int mark)
        {
            this.name = name;
            this.mark = mark;
        }

        public Subject(string name) : this(name, (int) MarksEnum.NotPassed)//здесь вызывается первый конструктор
        {
            this.name = name;
        }
        /// <summary>
        /// Свойства атрибутов name и mark
        /// </summary>
        public string Name
        {
            get { return name; }

            set { name = value; }
        }
        public MarksEnum Mark
        {
            get { return (MarksEnum) mark; }
            set { mark = (int) value; }
        }
        /// <summary>
        /// метод определяющий тип сдачи предмета
        /// </summary>
        /// <returns>зачет или экзамен</returns>
        public bool isExam()
        {
            bool point = mark > 1;
            return point;
        }
    }

    public class Student
    {
        private string fName, sName, mName;
        public Subject[] subjects = new Subject[5];    //массив ОБЪЕКТОВ класса Subject

        /// <summary>
        /// свойства
        /// </summary>
        public string FName
        {
            get { return fName; }
            set { fName = value; }
        }
        public string SName
        {
            get { return sName; }
            set { sName = value; }
        }
        public string MName
        {
            get { return mName; }
            set { mName = value; }
        }
        /// <summary>
        /// констуруктор
        /// </summary>
        public Student(string fName, string sName, string mName, Subject[] subjects)
        {
            this.fName = fName;
            this.sName = sName;
            this.mName = mName;
            this.subjects = subjects;
        }
        /// <summary>
        /// метод
        /// </summary>
        public void CopyMasSubject()
        {
            for (int i = 0; i < subjects.Length; i++)
            {
                Console.WriteLine(subjects[i].Name);
            }            
        }
    }
    internal class Program
        {
            public static void Main(string[] args)
            {
                Subject myObject = new Subject("Vasilina", 1);
                Console.WriteLine("My mark: " + myObject.Mark);
                Console.WriteLine(myObject.isExam());
                Subject[] twoSubject = new Subject[5];
                twoSubject[0] = new Subject("Математика");
                twoSubject[1] = new Subject("Физика");
                twoSubject[2] = new Subject("Русский язык");
                twoSubject[3] = new Subject("История");
                twoSubject[4] = new Subject("Информатика");
                Student student = new Student("Карташова", "Василина", "Владиславовна", twoSubject);
                student.CopyMasSubject();
            }
        }
    }