﻿using System;

namespace VasilinaLoveVlad
{
    public class Person
    {
        private string name;
        private int age;

        public string Name
        {
            get {return name};
            set { name = value; }
        }

        public int Age
        {
            get => age;
            set => age = value;
        }
    }

    public class Class1
    {
        public static void Main(string[] args)
        {
            Person personOne = new Person();
            Console.WriteLine(personOne.Age);
//            personOne.Age = 20;
        }
    }
}